from __future__ import unicode_literals
from django.conf import settings
from django.db import models

from django.utils import timezone


class Post (models.Model):
    title = models.CharField(max_length=120)
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return str(self.id)

    def __str__(self):
        return self.text



