import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from .models import Post
from .forms import CommentForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt


def post_detail(request, id):
    instance = get_object_or_404(Post, id=id)
    context = {
        "instance": instance,
    }
    return render(request, "post_detail.html", context)


def post_list(request):
    queryset_list = Post.objects.all().order_by("-timestamp")
    paginator = Paginator(queryset_list, 5)

    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:

        queryset = paginator.page(1)
    except EmptyPage:

        queryset = paginator.page(paginator.num_pages)

    context = {
        "object_list": queryset
    }
    return render(request, "index.html", context)


def post_api(request):
    queryset = Post.objects.all()
    queryset = serializers.serialize('json', queryset)
    return HttpResponse(queryset, content_type="application/json")

@csrf_exempt
def comment_api(request):   

    ret = [
     
        {'author': 'Andrew', 'text': 'Awesome'},
        {'author': 'Kirc', 'text': 'Boooo'},
        {'author': 'Vlad', 'text': 'Hello'},
        {'author': 'Alex', 'text': 'Good evening!'},
    ]

    if request.method == 'POST':
        author = request.POST.get("author")
        text = request.POST.get("text")
        obj = {
            'author': author,
            'text': text
        }
        ret += [obj] 

    return HttpResponse(json.dumps(ret), content_type="application/json")
    

  
    return HttpResponse(json.dumps(ret), content_type="application/json") 

def post_search (request):
    return render(request, "react_base.html")


    
