var SearchExample = React.createClass({

    getInitialState: function(){
        return { searchString: '' };
    },

    componentWillMount: function() {

        
    },

    handleChange: function(e){

        this.setState({searchString:e.target.value});
    },

    render: function() {

        var libraries = this.props.items,
            searchString = this.state.searchString.trim().toLowerCase();


        if(searchString.length > 0){

            libraries = libraries.filter(function(item){
                return item.fields.title.toLowerCase().match( searchString );
            });

        }


        return <div>
                <input type="text" value={this.state.searchString} onChange={this.handleChange} placeholder="Type here" />
                    <ul> 

                        { libraries.map(function(item){
                            return <li key={item.pk}><a href={'/' + item.pk}>{item.fields.title}</a></li>
                        }) }

                    </ul>

                </div>;

    }
});                                                                                                                                                       

function success(resp) {
            return resp.json()
        }
        function log(resp) {
            console.log(resp)
            ReactDOM.render(
                <SearchExample items={ resp } />,
                document.getElementById('container')
            );
        }
        fetch("http://127.0.0.1:8000/api/posts").then(success).then(log)


