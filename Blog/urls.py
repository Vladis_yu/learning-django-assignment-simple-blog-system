from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin
from myBlog import views as post_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', post_view.post_list),
    url(r'^(?P<id>\d+)/$', post_view.post_detail, name='post_detail'),
    url(r'^api/posts$', post_view.post_api),
    url(r'^api/comments/$', post_view.comment_api),
    url(r'^search/$', post_view.post_search),
]
